using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PatrolState : FSMState
{
    public PatrolState(Transform[] wp) 
    { 
        waypoints = wp;
        stateID = FSMStateID.Patrolling;

		FindNextPoint();

		curRotSpeed = 5;

		sceneGizmoColor = Color.green;
	}

    public override void Reason(List<Transform> targets, Transform target, NPCTankController npc)
    {
		//When the distance is near, transition to chase state
		foreach (Transform trans in targets)
		{
			if (Vector3.Distance(npc.transform.position, trans.position) <= 15.0f)
			{
				Debug.Log("Switch to Chase State");
				npc.currentTarget = trans;
				npc.SetTransition(Transition.SawPlayer);
			}
		}
    }

	public override void Act(List<Transform> targets, Transform player, NPCTankController npc)
	{
		//Find another random patrol point if the current point is reached
		if (Vector3.Distance(npc.transform.position, destPos) <= 2.0f)
        {
            FindNextPoint();
        }

		//Always Turn the turret towards the front of tank
		Transform turret = npc.GetComponent<NPCTankController>().turret;
		Quaternion turretRotation = Quaternion.LookRotation((npc.transform.position+npc.transform.forward) - turret.position);
		turret.rotation = Quaternion.Slerp(turret.rotation, turretRotation, Time.deltaTime * curRotSpeed);


		npc.navmeshAgent.SetDestination(destPos);
		npc.navmeshAgent.isStopped = false;
	}

	public override void DoBeforeEntering(NPCTankController npc)
	{
		
	}

	public override void DoBeforeLeaving(NPCTankController npc)
	{
		
	}


}