using UnityEngine;
using System.Collections;

public class NPCTankController : AdvancedFSM 
{
    public GameObject Bullet;
    private int health;
	public Transform gizmo;

    //Initialize the Finite state machine for the NPC tank
    protected override void Initialize()
    {
        health = 100;

        elapsedTime = 0.0f;
        shootRate = 2.0f;

		GameObject[] gos = GameObject.FindGameObjectsWithTag("tank") as GameObject[];

		foreach(GameObject go in gos)
		{
			if (go != this.gameObject)
			{
				targets.Add(go.transform);
			}
		}

        //if (!currentTarget)
           // print("Player doesn't exist.. Please add one with Tag named 'Player'");

		navmeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        //Start Doing the Finite State Machine
        ConstructFSM();
    }

    //Update each frame
    protected override void FSMUpdate()
    {
        //Check for health
        elapsedTime += Time.deltaTime;
    }

    protected override void FSMFixedUpdate()
    {
        CurrentState.Reason(targets, currentTarget, this);
        CurrentState.Act(targets, currentTarget, this);
    }

    public void SetTransition(Transition t) 
    { 
        PerformTransition(t, this); 
    }

    private void ConstructFSM()
    {
        //Get the list of points
        waypointList = GameObject.FindGameObjectsWithTag("waypoint");

        Transform[] waypoints = new Transform[waypointList.Length];
        int i = 0;
        foreach(GameObject obj in waypointList)
        {
            waypoints[i] = obj.transform;
            i++;
        }

        PatrolState patrol = new PatrolState(waypoints);
        patrol.AddTransition(Transition.SawPlayer, FSMStateID.Chasing);
        patrol.AddTransition(Transition.NoHealth, FSMStateID.Dead);

        ChaseState chase = new ChaseState(waypoints);
        chase.AddTransition(Transition.LostPlayer, FSMStateID.Patrolling);
        chase.AddTransition(Transition.ReachPlayer, FSMStateID.Attacking);
        chase.AddTransition(Transition.NoHealth, FSMStateID.Dead);

        AttackState attack = new AttackState(waypoints);
        attack.AddTransition(Transition.LostPlayer, FSMStateID.Patrolling);
        attack.AddTransition(Transition.SawPlayer, FSMStateID.Chasing);
        attack.AddTransition(Transition.NoHealth, FSMStateID.Dead);

        DeadState dead = new DeadState();
        dead.AddTransition(Transition.NoHealth, FSMStateID.Dead);

        AddFSMState(patrol);
        AddFSMState(chase);
        AddFSMState(attack);
        AddFSMState(dead);
    }

    /// <summary>
    /// Check the collision with the bullet
    /// </summary>
    /// <param name="collision"></param>
    void OnCollisionEnter(Collision collision)
    {
        //Reduce health
        if (collision.gameObject.tag == "Bullet")
        {
            health -= 50;

            if (health <= 0)
            {
                Debug.Log("Switch to Dead State");
                SetTransition(Transition.NoHealth);
                Explode();
            }
        }
    }

    protected void Explode()
    {
        float rndX = Random.Range(10.0f, 30.0f);
        float rndZ = Random.Range(10.0f, 30.0f);
        for (int i = 0; i < 3; i++)
        {
            GetComponent<Rigidbody>().AddExplosionForce(10000.0f, transform.position - new Vector3(rndX, 10.0f, rndZ), 40.0f, 10.0f);
            GetComponent<Rigidbody>().velocity = transform.TransformDirection(new Vector3(rndX, 20.0f, rndZ));
        }

        Destroy(gameObject, 1.5f);
    }

    /// <summary>
    /// Shoot the bullet from the turret
    /// </summary>
    public void ShootBullet()
    {
        if (elapsedTime >= shootRate)
        {
            Instantiate(Bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
            elapsedTime = 0.0f;
        }
    }

	public void RemoveFromList(GameObject tank)
	{
		targets.Remove(tank.transform);
		
	}

	private void OnDrawGizmos()
	{
		switch (CurrentStateID)
		{
			case FSMStateID.Patrolling:
				Gizmos.color = CurrentState.sceneGizmoColor;
				Gizmos.DrawWireSphere(gizmo.position, 0.5f);
				break;
			case FSMStateID.Chasing:
				Gizmos.color = CurrentState.sceneGizmoColor;
				Gizmos.DrawWireSphere(gizmo.position, 0.5f);
				break;
			case FSMStateID.Attacking:
				Gizmos.color = CurrentState.sceneGizmoColor;
				Gizmos.DrawWireSphere(gizmo.position, 0.5f);
				break;
		}
	}
}
